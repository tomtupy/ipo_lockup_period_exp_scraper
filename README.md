# SEC EDGAR IPO Lockup Period Expirations Scraper
This package scrapes the current month's lockup period expirations 
from the SEC EDGAR website.

# Testing
pipenv install
pipenv run pytest
